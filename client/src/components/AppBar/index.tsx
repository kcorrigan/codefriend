// PACKAGES
import React from 'react';
// UTILITIES
import {Desktop, Tablet, Mobile, Default} from '../../Utils/responsive-utils';
// COMPONENTS
import Icon from '../Icon';
import Link from '../Link';
// STYLES
require('./styles.scss');
const classNames = require('classnames');

// TYPES
interface AppBarState {
    selectedTab: Number | null;
}

interface AppBarProps { }

class AppBar extends React.Component<{}, AppBarState> {
    // LIFECYCLE
    constructor(props: AppBarProps) {
        super(props)

        // Set Initial State
        this.state = {
            selectedTab: 0,
        }
    }
    
    // HANDLERS
    handleAppBarLinkClick = (idx: Number) => {
        this.setState({ selectedTab: idx });
    }

    // RENDER
    render () {
        return (
            <div className="app-bar">
                <Desktop>{this.renderDesktopLinks()}</Desktop>
            </div>
        )
    }

    renderDesktopLinks () {
        const { selectedTab } = this.state;
        
        return links.map((link, idx) => {
            let linkClass = classNames(
                'app-bar__link',
                'm-lg-l',
                'm-lg-r',
            )

            if (selectedTab === idx) {
                linkClass += ' app-bar__link--selected'
            } else {
                linkClass += ' link-underline' 
            }

            return (
                <Link 
                    className={linkClass}
                    onClick={this.handleAppBarLinkClick.bind(null, idx)}
                    key={link + idx}
                >
                    {link}
                </Link>
            );
        });
    }
}

// PRIVATE
const links = [
    "Blog",
    "Tutorials",
    "Projects",
    "Interview Prep",
    "About Us",
];

// EXPORTS
export default AppBar;
