// PACKAGES
import React from 'react';
// STYLES
require('./styles.scss');
const classNames = require('classnames');

// TYPES
interface DropdownState { 
    isOpen: Boolean;
}

interface DropdownProps {
    list: Array<string>;
    title: String | Object;
    classes?: String | Array<String> | undefined;
}

class Dropdown extends React.Component<DropdownProps, DropdownState> {
    // LIFECYCLE
    constructor(props: DropdownProps) {
        super(props);

        // Initial State
        this.state = {
            isOpen: false,
        };
    }

    onToggleList = () => {
        const { isOpen } = this.state;

        this.setState({
            isOpen: !isOpen,
        });
    }

    render () {
        const { 
            list,
            title,
            classes,
        } = this.props; 
        const { isOpen } = this.state;

        return (
            <div className="dropdown">
                <div
                    className={
                        classNames(
                            classes ||
                            "dropdown__title",
                        )
                    }  
                    onClick={this.onToggleList}
                >
                    {title}
                </div>
                {isOpen &&
                    <div className="dropdown__list m-md-t">
                        {list.map(item => {
                            return(
                                <div className={
                                    classNames(
                                        "dropdown__list-item",
                                        "text-x-lg",
                                        "p-md-b",
                                        "p-md-t",
                                    )
                                }
                                >
                                    {item}
                                </div>
                            )
                        })}
                    </div>
                }
            </div>
        );
    }
 
}

// EXPORTS
export default Dropdown;
