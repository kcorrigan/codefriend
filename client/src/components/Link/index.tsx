// PACKAGES
import React, { Component, ReactNode }  from 'react';
// STYLES
require('./styles.scss');

// TYPES
interface LinkProps {
    className: string;
    onClick: () => void;
    key: string;
    children: ReactNode;
}

class Link extends Component <LinkProps, {}> {
    constructor (props: LinkProps) {
        super(props);
    }

    render () {
        const { children, className, key, onClick} = this.props;
        return (
            <div
                className={className}
                key={key}
                onClick={onClick}
            >
                {children}
            </div>
        );
    }
}

export default Link;