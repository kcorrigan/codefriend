// PACKAGES
import React from 'react';
// UTILITIES
import {Desktop, Tablet, Mobile, Default} from '../../Utils/responsive-utils';
// COMPONENTS
import Icon from '../Icon';
import Dropdown from '../Dropdown';
// LIBS
import { homeLink } from '../../libs/constants.js';
// STYLES
require('./styles.scss');
const classNames = require('classnames');

// TYPES
interface HeaderState { 
    dropdownEnabled: boolean;
}
interface HeaderProps { }

class Header extends React.Component<{}, HeaderState> {
    // LIFECYCLE
    constructor (props: HeaderProps) {
        super(props);

        // Set initial state
        this.state = {
            dropdownEnabled: false,
        }
    }
    // RENDER
    render () {
        return (
            <div className="header">
                <Desktop>{this.renderDesktopHeader()}</Desktop>
                <Mobile>{this.renderMobileHeader()}</Mobile>
            </div>
        );
    }

    renderDesktopHeader () {
        return (
            <div className="header flex-row">
                {this.renderLogo()}
                <div className="header__icons flex-row">
                    <Icon 
                        icon={['fab', 'twitter']}
                        size="2x"
                        className="m-lg-r"
                    />
                </div>
            </div>
        );
    }

    renderLogo() {
        return (
            <img
                className={classNames('header__logo', 'm-lg-l')}
                src={homeLink}
                alt="code friend logo"
            />
        );
    }

    renderMobileHeader () {
        const { dropdownEnabled } = this.state; 
        const dropdownIcon = (
            <Icon
                className={classNames('header__mobile-dropdown', 'm-lg-r')}
                icon="bars"
                size="2x"
            />
        );

        return (
            <div className="header flex-row">
                {this.renderLogo()}
                <Dropdown
                    title = {dropdownIcon}
                    list={links}
                />
            </div>
        )
    }
}

// PRIVATE
const links = [
    "Blog",
    "Tutorials",
    "Projects",
    "Interview Prep",
    "About Us",
];

// EXPORTS
export default Header;
