// PACKAGES
import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

// TYPES
type FontAwesomeSize = 'lg' | '2x' | '3x' | '4x' | '5x';
type FontAwesomeStack = '1x' | '2x';
type FontAwesomeFlip = 'horizontal' | 'vertical';

interface FontAwesomeProps {
    icon: any;
    color?: string;
    className?: string;
    ariaLabel?: string;
    border?: boolean;
    cssModule?: any;
    fixedWidth?: boolean;
    flip?: FontAwesomeFlip;
    inverse?: boolean;
    name?: string;
    pulse?: boolean;
    rotate?: number;
    size?: FontAwesomeSize;
    spin?: boolean;
    stack?: FontAwesomeStack;
    tag?: string;
    onClick?: () => void;
}

const Icon: React.SFC<FontAwesomeProps> = (props) => {
    return (
        <FontAwesomeIcon 
            icon={props.icon}
            color={props.color}
            className={props.className}
            size={props.size}
            border={props.border}
            fixedWidth={props.fixedWidth}
            flip={props.flip}
            inverse={props.inverse}
            pulse={props.pulse}
            spin={props.spin}
            onClick={props.onClick}   
        />
    );
};

// EXPORTS
export default Icon;
