// PACKAGES
import React, { Component } from 'react';
// COMPONENTS
import Header from '../components/Header';
import AppBar from '../components/AppBar';

class Main extends Component {
  componentDidMount () {
    try {
      fetch('/test')
      .then(res => console.log(res.json()))         
    } catch(err) {
      console.log('I\'m an error', err);
    }
  }

  render() {
    return (
      // import google fonts
      // search bar on far right with social media link icons next to search bar
      // set up font awesome
      <div className="App">
        <Header/>
        <AppBar/>
      </div>
    );
  }
}

export default Main;
