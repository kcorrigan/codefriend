// PACKAGES
import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
// UTILS
import { initializeIcons} from './Utils/icon-utils';
// COMPONENTS
import Main from './containers/Main';

// On app startup, add icons to font awesome library
initializeIcons();

class App extends Component {
  render() {
    const App = () => (
      <div>
          <BrowserRouter> 
            <Switch>
                <Route exact path='/' component={Main}/>
            </Switch>
          </BrowserRouter>
      </div>
    )

    return (
        <App/>
    );
  }
}

export default App;
