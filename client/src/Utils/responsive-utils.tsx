import { useMediaQuery } from 'react-responsive';

interface MediaProps {
  children: any;
}

export const Desktop = ({ children, ...props }: MediaProps) => {
  const isDesktop = useMediaQuery({ minWidth: 992 })
  return isDesktop ? children : null
}

export const Tablet = ({ children, ...props }: MediaProps) => {
  const isTablet = useMediaQuery({ minWidth: 768, maxWidth: 991 })
  return isTablet ? children : null
}

export const Mobile = ({ children, ...props }: MediaProps) => {
  const isMobile = useMediaQuery({ maxWidth: 767 })
  return isMobile ? children : null
}

export const Default = ({ children, ...props }: MediaProps) => {
  const isNotMobile = useMediaQuery({ minWidth: 768 })
  return isNotMobile ? children : null
}

