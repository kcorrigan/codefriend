// PACKAGES
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
// ICON IMPORTS
import { 
    faCheckSquare,
    faCoffee,
    faBars
} from '@fortawesome/free-solid-svg-icons'

export const initializeIcons = () => {
    library.add(fab, {...icons})
}

// PRIVATE
const icons = {
    faCheckSquare,
    faCoffee,
    faBars,
}