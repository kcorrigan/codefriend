"use strict";
const express = require('express');
const path = require('path');
const app = express();
const port = process.env.PORT || 8080;
app.use(express.static(path.join(__dirname, '../client/build')));
app.get('/*', (req, res) => {
    res.sendFile(path.join(__dirname, '../client/build/index.html'));
});
app.get("/test", (req, res) => {
    res.json({ body: "we did it" });
});
app.listen(port, () => {
    console.log(`server started at port: ${port}`);
});
//# sourceMappingURL=index.js.map