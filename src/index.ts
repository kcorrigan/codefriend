// IMPORTS
const express = require('express');
const path = require('path')

//PRIVATE
const app = express();
const port = process.env.PORT || 8080; // default port to listen

app.use(express.static(path.join(__dirname, '../client/build')));
app.get('/*', (req: any, res: any) => {
  res.sendFile(path.join(__dirname, '../client/build/index.html'));
});

app.get("/test", (req: any, res: any) => {
    res.json({ body: "we did it" });
});

// start the Express server
app.listen(port, () => {
    console.log( `server started at port: ${port}` );
});

